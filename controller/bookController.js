const BookSchema = require("../models/Book");
const books = require("../database/books.js");

const addBooks = (req, res, next) => {
  BookSchema.insertMany(books)
    .then((response) => {
      res.status(201).json({
        message: "Books successfully created!",
        result: response,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Book.",
      });
    });
};

const createBook = (req, res, next) => {
  if (!req.body.name) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  const book = new BookSchema({
    name: req.body.name,
    description: req.body.description,
    category: req.body.category,
    thumbnail: req.body.thumbnail,
  });
  book
    .save()
    .then((response) => {
      res.status(201).json({
        message: "Book successfully created!",
        result: response,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Book.",
      });
    });
};

const getAllBooks = (req, res, next) => {
  BookSchema.find()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving books.",
      });
    });
};

const getAllBooksCategory = (req, res, next) => {
  const tagCategory = req.params.category;
  BookSchema.find({ category: { $in: [tagCategory] } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving books.",
      });
    });
};

const updateBook = (req, res, next) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  BookSchema.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Book with id=${id}. Maybe Book was not found!`,
        });
      } else res.send({ message: "Book was updated successfully." });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Book with id=" + id,
      });
    });
};

const deleteBook = (req, res, next) => {
  const id = req.params.id;

  BookSchema.findByIdAndRemove(id, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Book with id=${id}. Maybe Book was not found!`,
        });
      } else {
        res.send({
          message: "Book was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Book with id=" + id,
      });
    });
};

module.exports = {
  addBooks,
  createBook,
  getAllBooks,
  getAllBooksCategory,
  updateBook,
  deleteBook,
};
