const books = [
  {
    name: "To Kill A Mockingbird",
    description:
      "To Kill a Mockingbird is a novel by the American author Harper Lee. It was published in 1960 and was instantly successful. In the United States, it is widely read in high schools and middle schools. To Kill a Mockingbird has become a classic of modern American literature, winning the Pulitzer Prize.",
    category: ["Fiction", "Bildungsroman"],
    thumbnail: "tkam.jpg",
  },
  {
    name: "The Sun Also Rises",
    description:
      "The Sun Also Rises is a 1926 novel by American writer Ernest Hemingway, his first, that portrays American and British expatriates who travel from Paris to the Festival of San Fermín in Pamplona to watch the running of the bulls and the bullfights.",
    category: ["Fiction", "Novel"],
    thumbnail: "tsar.jpg",
  },
];

module.exports = books;
