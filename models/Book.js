const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let bookSchema = new Schema(
  {
    name: {
      type: String,
    },
    description: {
      type: String,
    },
    category: [
      {
        type: String,
      },
    ],
    thumbnail: {
      type: String,
    },
  },
  {
    collection: "books",
  }
);

module.exports = mongoose.model("Book", bookSchema);
