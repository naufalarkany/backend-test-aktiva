const express = require("express");
const bookRouter = require("./bookRouter.js");
const router = express.Router();

/* GET home page. */
router.use("/book", bookRouter);
router.get("/", (req, res, next) => {
  res.send("Home");
});

module.exports = router;
