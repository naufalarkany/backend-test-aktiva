const express = require("express");
const router = express.Router();
const bookController = require("../controller/bookController.js");

/* GET home page. */
router.get("/", bookController.getAllBooks);

router.get("/in/", bookController.getAllBooksCategory);

router.post("/addBooks", bookController.addBooks);

router.post("/create", bookController.createBook);

router.patch("/update/:id", bookController.updateBook);

router.delete("/delete/:id", bookController.deleteBook);

module.exports = router;
